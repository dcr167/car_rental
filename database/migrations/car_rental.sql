-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-05-2019 a las 09:25:00
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `car_rental`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `car`
--

CREATE TABLE `car` (
  `id_car` bigint(20) UNSIGNED NOT NULL,
  `name_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car_year` year(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `car`
--

INSERT INTO `car` (`id_car`, `name_car`, `model_car`, `car_year`, `created_at`, `updated_at`) VALUES
(1, 'corvete', 'r9', 2017, NULL, NULL),
(2, 'ferrari', '770', 2015, '2019-05-31 04:00:00', '2019-05-31 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id_client` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `lastname` text COLLATE utf8mb4_unicode_ci,
  `ci` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id_client`, `name`, `lastname`, `ci`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Juanito', 'Arcoiris', 59546798, 'hello world', '2019-05-29 04:00:00', '2019-05-29 04:00:00'),
(2, 'Edward', 'Elrick', 215499, 'Lol', '2019-05-30 04:00:00', '2019-05-31 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cost`
--

CREATE TABLE `cost` (
  `id_cost` bigint(20) UNSIGNED NOT NULL,
  `price` int(11) DEFAULT NULL,
  `descrip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cost`
--

INSERT INTO `cost` (`id_cost`, `price`, `descrip`, `created_at`, `updated_at`) VALUES
(1, 80000, 'sadasfds', '2019-05-30 04:00:00', '2019-05-30 04:00:00'),
(2, 500000, 'Es un buen coche', '2019-05-31 04:00:00', '2019-05-31 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detail`
--

CREATE TABLE `detail` (
  `id_detail` bigint(20) UNSIGNED NOT NULL,
  `date_rent` date DEFAULT NULL,
  `time_rent` date DEFAULT NULL,
  `cant_car_rent` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `detail`
--

INSERT INTO `detail` (`id_detail`, `date_rent`, `time_rent`, `cant_car_rent`, `created_at`, `updated_at`) VALUES
(1, '2019-05-29', '2019-05-31', 1, '2019-05-31 04:00:00', '2019-05-31 04:00:00'),
(2, '2019-05-27', '2019-05-28', 7, '2019-05-30 04:00:00', '2019-05-31 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `madecar`
--

CREATE TABLE `madecar` (
  `id_made` bigint(20) UNSIGNED NOT NULL,
  `date_made` date DEFAULT NULL,
  `name_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `madecar`
--

INSERT INTO `madecar` (`id_made`, `date_made`, `name_car`, `model_car`, `created_at`, `updated_at`) VALUES
(1, '2019-05-23', 'vitara', '770', '2019-05-30 04:00:00', '2019-05-31 04:00:00'),
(2, '2019-05-27', 'lamborgini veneno', 'r9290', '2019-05-30 04:00:00', '2019-05-31 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_05_30_073340_create_car_table', 1),
(2, '2019_05_30_073528_create_client_table', 1),
(3, '2019_05_30_073709_create_user_table', 1),
(4, '2019_05_30_074020_create_madecar_table', 1),
(5, '2019_05_30_074124_create_detail_table', 1),
(6, '2019_05_30_074205_create_order_table', 1),
(7, '2019_05_30_074419_create_cost_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

CREATE TABLE `order` (
  `id_order` bigint(20) UNSIGNED NOT NULL,
  `day_order` date DEFAULT NULL,
  `time_order` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order`
--

INSERT INTO `order` (`id_order`, `day_order`, `time_order`, `created_at`, `updated_at`) VALUES
(1, '2019-05-01', '2019-05-16', '2019-05-16 04:00:00', '2019-05-31 04:00:00'),
(2, '2019-05-09', '2019-05-24', '2019-05-29 04:00:00', '2019-05-31 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `lastname` text COLLATE utf8mb4_unicode_ci,
  `ci` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `name`, `lastname`, `ci`, `address`, `created_at`, `updated_at`) VALUES
(1, 'nero290', '123', 'Luis', 'Cabrera', 78979848, 'Lol', '2019-05-30 04:00:00', '2019-05-31 04:00:00'),
(2, 'vash', '6667', 'Trigun', 'Larson', 4849, 'lara', '2019-05-30 04:00:00', '2019-05-31 04:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id_car`);

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`);

--
-- Indices de la tabla `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`id_cost`);

--
-- Indices de la tabla `detail`
--
ALTER TABLE `detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indices de la tabla `madecar`
--
ALTER TABLE `madecar`
  ADD PRIMARY KEY (`id_made`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `car`
--
ALTER TABLE `car`
  MODIFY `id_car` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `id_client` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cost`
--
ALTER TABLE `cost`
  MODIFY `id_cost` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `detail`
--
ALTER TABLE `detail`
  MODIFY `id_detail` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `madecar`
--
ALTER TABLE `madecar`
  MODIFY `id_made` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `order`
--
ALTER TABLE `order`
  MODIFY `id_order` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
