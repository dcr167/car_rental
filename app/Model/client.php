<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    protected $table= 'client';
    protected $primarykey='id_client';

    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'name',
        'lastname',
        'ci',
        'address',

    ];
}
