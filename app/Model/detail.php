<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class detail extends Model
{
    protected $table= 'detail';
    protected $primarykey= 'id_detail';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'date_rent',
        'time_rent',
        'cant_car_rent',

    ];
}
