<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class car extends Model
{
    protected $table= 'car';
    protected $primarykey= 'id_car';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'name_car',
        'model_car',
        'car_year',

    ];
}
