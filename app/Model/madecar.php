<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class madecar extends Model
{
    protected $table= 'madecar';
    protected $primarykey= 'id_made';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'date_made',
        'name_car',
        'model_car',

    ];
}
