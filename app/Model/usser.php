<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class usser extends Model
{
    protected $table= 'user';
    protected $primarykey= 'id_user';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'username',
        'password',
        'name',
        'lastname',
        'ci',
        'address',

    ]; 
}
