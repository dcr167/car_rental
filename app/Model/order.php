<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table= 'order';
    protected $primarykey= 'id_order';
    public $timestamps= true;
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_upadate';

    protected $fillable= [
        'day_order',
        'time_order',

    ]; 
}
